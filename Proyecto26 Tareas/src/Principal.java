import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Principal {
	static BufferedReader leer=new BufferedReader(new InputStreamReader(System.in));

	public static void main(String[] args) {
		GestorTareas gestor;
		try {
			gestor = new GestorTareas();
			int opc;
			do {
				do {
					System.out.println("***MENU***");
					System.out.println("1 Crear tarea");
					System.out.println("2 Crear detalle a tarea");
					System.out.println("3 Mostrar tarea");
					System.out.println("4 Cambiar detalle a realizado");
					System.out.println("5 Listar todas las tareas");
					System.out.println("6 Salir");
					try {
						opc=Integer.parseInt(leer.readLine());
					} catch (NumberFormatException | IOException e) {
						opc=0;
					}
				}while(opc<1||opc>6);
				
				switch(opc) {
					case 1: gestor.crearTarea();
							break;
					case 2: gestor.crearDetalle();
							break;
					case 3: gestor.mostrarDetalle();					
							break;
					case 4: gestor.realizarDetalle();					
							break;
					case 5: gestor.listarTareas();				
							break;
					case 6: System.out.println("Adios");
							break;
							
				}
			}while(opc!=6);
			gestor.cerrarConexion();
		} catch (SQLException e) {
			System.out.println("Ha habido un problema con la conexion");
			e.printStackTrace();
		}

	}



}
