import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Tarea {
	private int id;
	private String nombre;
	private ArrayList<Detalle> detalles=new ArrayList();
	
	transient BufferedReader leer=new BufferedReader(new InputStreamReader(System.in));
	public Tarea(int id, String nombre, ArrayList<Detalle> detalles) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.detalles = detalles;
	}
	public Tarea(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	public Tarea() {
		super();
		detalles.clear();
	}


	public void mostrarTarea() {
		System.out.println("ID: "+id);
		System.out.println("NOMBRE: "+nombre);
		System.out.println("DETALLES: ");
		for (int i = 0; i < detalles.size(); i++) {
			detalles.get(i).mostrarDetalle();
		}
	}

	public void cargarTarea(Connection conexion,int i) {
		try {
			Statement consulta=conexion.createStatement();
			ResultSet con=consulta.executeQuery("select * from tareas where id="+i);
			if(con.next()) {
				id=con.getInt(1);
				nombre=con.getString(2);
				Statement consulta2=conexion.createStatement();
				ResultSet con2=consulta2.executeQuery("select * from detalle where idTarea="+i);
				while(con2.next()) {
					int idDet=con2.getInt(1);
					int idTar=con2.getInt(2);
					String nombr=con2.getString(3);
					boolean real=con2.getBoolean(4);
					
					Detalle d=new Detalle(idDet,idTar,nombr,real);
					detalles.add(d);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public ArrayList<Detalle> getDetalles() {
		return detalles;
	}
	public void setDetalles(ArrayList<Detalle> detalles) {
		this.detalles = detalles;
	}
	

}
