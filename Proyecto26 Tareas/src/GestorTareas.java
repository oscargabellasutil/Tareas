import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GestorTareas {
	
	private Connection conn;
	private Tarea t;
	private Detalle d;
	//private int id;
	private boolean existe;
	
	transient BufferedReader leer=new BufferedReader(new InputStreamReader(System.in));
	
	GestorTareas() throws SQLException{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException ex) {
			System.out.println("No existe el driver mysql");
		}
		conn=DriverManager.getConnection("jdbc:mysql://localhost/tareas","root","12345");
		try {
			
			conn=DriverManager.getConnection("jdbc:mysql://localhost/tareas","root","12345");
			System.out.println("Conexi�n realizada correctamente");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t=new Tarea();
		d=new Detalle();
		existe=false;
		
	}
	public String pedirTarea() throws IOException {
		String nombre;
		do {
			System.out.println("Introduce el nombre de la tarea");
			nombre=leer.readLine();
		}while(nombre.length()==0);
		return nombre;
	}
	public void guardarBaseDatosTarea(String nom) {
		
		PreparedStatement ps;
		Statement st;
		try {
			/*conexion.setAutoCommit(false);
			conexion.commit();
			PAra cuando se quiere modificar varias tablas a la vez y sucedan al mismo tiempo y no secuencialmente
			*/
			st=conn.createStatement();
			st.executeUpdate("INSERT INTO tareas (`nombre`) VALUES ('"+nom+"')");
			//ps = conn.prepareStatement("INSERT INTO tareas (`nombre`) VALUES ('"+nom+"')");
			
			//ps.setString(1, nom);
			System.out.println("Se ha a�adido la tarea "+nom);
			//ps.executeUpdate();
		} catch (SQLException e) {
			System.out.println("No se ha podido a�adir la tarea "+nom);
			e.printStackTrace();
		}

		
	}
	public void crearTarea() {
		String nombre;
		try {
			nombre=pedirTarea();
			guardarBaseDatosTarea(nombre);
		} catch (IOException e) {
			System.out.println("No se ha podido crear la tarea");
			e.printStackTrace();
		}
		
	}
	public boolean existeTarea(int id) {
		boolean ex=false;
		try {
			Statement consulta=conn.createStatement();
			ResultSet con=consulta.executeQuery("select * from tareas where id="+id);
			if(con.next()) {
				ex= true;
			}

		} catch (SQLException e) {
			System.out.println("No se ha podido comprobar si existe la tarea");
			e.printStackTrace();
		}
		return ex;
	}
	public void guardarBaseDatosDetalle(int idTarea,String nombre,boolean realizado) {
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement("INSERT INTO `detalle`( `idTarea`, `nombre`, `realizado`) VALUES (?,?,?)");
			ps.setInt(1, idTarea);
			ps.setString(2, nombre);
			ps.setBoolean(3, realizado);
			ps.executeUpdate();
			/*
			Statement con=conexion.createStatement();
			if(realizado)
				con.executeUpdate("INSERT INTO `detalle`( `idTarea`, `nombre`, `realizado`) VALUES ("+idTarea+",'"+nombre+"','"+1+"')");
			else con.executeUpdate("INSERT INTO `detalle`( `idTarea`, `nombre`, `realizado`) VALUES ("+idTarea+",'"+nombre+"','"+0+"')");
			*/
		} catch (SQLException e) {
			System.out.println("No se ha podido insertar");
			e.printStackTrace();
		}

		System.out.println("Se ha insertado correctamente la tarea "+nombre);
	}
	public void crearDetalle() {
			int id;
			try {
				id=pedirId("El id de la tarea a la que a�adir detalle");
				t.setId(id);
				existe=existeTarea(id);
				if(existe) {
					d.pedirDetalle(id);
					guardarBaseDatosDetalle(d.getIdTarea(),d.getNombre(),d.isRealizado());
				}
				else {
					System.out.println("El id de tarea introducido no se corresponde con ninguno de la base de datos");
				}
			} catch (IOException e) {
				System.out.println("No se ha podido crear el detalle");
				e.printStackTrace();
			}
			
	}
	private int pedirId(String texto) {
		int id;
		do {
			System.out.println(texto);
			try {
				id=Integer.parseInt(leer.readLine());
			} catch (NumberFormatException e) {
				id=0;
			} catch (IOException e) {
				id=0;
			}
		}while(id<1);
		return id;
	}
	public Tarea cargarTarea(int i) {
		Tarea ta = null;
		int id;
		ArrayList<Detalle> deta=new ArrayList();
		String nombre = null;
		try {
			Statement consulta=conn.createStatement();
			ResultSet con=consulta.executeQuery("select * from tareas where id="+i);
			if(con.next()) {
				id=con.getInt(1);
				nombre=con.getString(2);
				Statement consulta2=conn.createStatement();
				ResultSet con2=consulta2.executeQuery("select * from detalle where idTarea="+i);
				while(con2.next()) {
					int idDet=con2.getInt(1);
					int idTar=con2.getInt(2);
					String nombr=con2.getString(3);
					boolean real=con2.getBoolean(4);
					
					Detalle d=new Detalle(idDet,idTar,nombr,real);
					deta.add(d);
				}
			}
			ta=new Tarea(i,nombre,deta);

		} catch (SQLException e) {
			System.out.println("No se ha podido cargar la tarea");
			e.printStackTrace();
		}
		return ta;
	
	}
	public void mostrarDetalle() {
			Tarea ta;
			int id;
			id=pedirId("El id de la tarea a mostrar");
			t.setId(id);
			existe=existeTarea(id);
			if(existe) {
				ta=cargarTarea(id);
				ta.mostrarTarea();
			}
			else {
				System.out.println("El id de tarea introducido no se corresponde con ninguno de la base de datos");
			}	

	}
	public boolean existeDetalle(int id) {
		boolean ex=false;
		try {
			Statement consulta=conn.createStatement();
			ResultSet con=consulta.executeQuery("select * from detalle where id="+id);
			if(con.next()) {
				ex= true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ex;
	}
	public void detalleRealizado(int id) {
		
		try {
			Statement modificacion=conn.createStatement();
			modificacion.executeUpdate("Update detalle set realizado=1 where id="+id);
			System.out.println("Se ha marcada como realizada la actividad "+id);
		} catch (SQLException e) {
			System.out.println("No se ha podido realizar la modificaci�n del detalle "+id+" para asignarle que est� realizado");
			e.printStackTrace();
		}
	}
	public void realizarDetalle() {
		int id;
		id=pedirId("El id del detalle a modificar");
		d=new Detalle(id);
		existe=existeDetalle(id);
		if(existe) {
			detalleRealizado(id);
		}
		else {
			System.out.println("El id del detalle introducido no se corresponde con ninguno de la base de datos");
		}
	}
	public void listarTareas() {
		try {	
			Statement consulta=conn.createStatement();
			ResultSet con=consulta.executeQuery("select id from tareas");
			while(con.next()){
				t=new Tarea();
				t.cargarTarea(conn, con.getInt(1));
				t.mostrarTarea();
			}
		}catch(SQLException e) {
			System.out.println("No se han podido mostrar las tareas");
		};
	}
	public void cerrarConexion() {
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("No se han podido cerrar la conexi�n");
			e.printStackTrace();
		}
	}
	
	

}
