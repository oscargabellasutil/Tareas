import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Detalle {
	private int id;
	private int idTarea;
	private String nombre;
	private boolean realizado;
	transient BufferedReader leer=new BufferedReader(new InputStreamReader(System.in));
	
	public Detalle(int id) {
		super();
		this.id = id;
	}
	public Detalle(int id, int idTarea, String nombre, boolean realizado) {
		super();
		this.id = id;
		this.idTarea = idTarea;
		this.nombre = nombre;
		this.realizado = realizado;
	}
	public void pedirDetalle(int tarea) throws IOException {
		idTarea=tarea;
		do {
			System.out.println("Introduzca el nombre del detalle");
			nombre=leer.readLine();
		}while(nombre.length()==0);
		int real;
		do {
			System.out.println("Introduzca un 1 si se ha relizado "+nombre+" 2 en caso contrario");
			real=Integer.parseInt(leer.readLine());
		}while(real!=1&&real!=2);
		if(real==1)
			realizado=true;
		else realizado=false;
	}

	public void mostrarDetalle() {
		System.out.println("Id detalle: "+id);
		System.out.println("Id tarea: "+idTarea);
		System.out.println("Nombre detalle: "+nombre);
		if(realizado)
			System.out.println("Realizada: SI");
		else System.out.println("Realizada: NO");
	}

	public Detalle() {
		super();
		realizado=false;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isRealizado() {
		return realizado;
	}
	public void setRealizado(boolean realizado) {
		this.realizado = realizado;
	}
	

}
